console.log("hello world");

// assignement operator =

let assignmentnumber = 8;
console.log(assignmentnumber);


// addition assignment operator +=

assignmentnumber = assignmentnumber + 2;
console.log(assignmentnumber);

assignmentnumber = assignmentnumber + 2;
console.log(assignmentnumber);

assignmentnumber += 2;
console.log(assignmentnumber);

assignmentnumber += 2;
console.log(assignmentnumber);

// same with subtraction, multiplication, division

assignmentnumber /= 2;
console.log(assignmentnumber);

assignmentnumber -= 2;
console.log(assignmentnumber);

assignmentnumber *= 3;
console.log(assignmentnumber);


// arithmetic operators + - / * %

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("result of mdas operation " + mdas);
// pemdas
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("result of mdas operation " + pemdas);



// increments ++ add 1
// decrements -- minus 1

// prefix and postfix

let z = 1;
++z
console.log(z);

z++
console.log(z);

console.log(z++);
console.log(z);

console.log(++z); 
// nag add muna ng 1 saka inadd si z



// prefix and postfix

console.log(--z);
// 5-1

console.log(z--);
console.log(z);
// return value muna bago bawasan





// type coercion

let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
// string, concatenate - conversion of values
console.log(typeof coercion);

// 
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
// number to number = no conversion


let numE = true + 1;
console.log(numE);
console.log(typeof numE);
// number

// bolean : true = 1; false = 0;



// comparison operator
//  == equality
let juan = "juan";

console.log("equality operator");
console.log(1==1);
 // true
console.log(1==2);
 // false
console.log(1=="1")
 // true
 console.log(0==false);
 // true
console.log("juan"=="JUAN");
// false, js is case-sensitive
console.log("juan"==juan);
// true

// strict equality ===
console.log("strict equality operator");
console.log(1===1);
 // true
console.log(1===2);
 // false
console.log(1==="1")
 // false. data-type
 console.log(0===false);
 // false. number-bolean
console.log("juan"==="JUAN");
// false, js is case-sensitive
console.log("juan"===juan);
// true



// inequality operator !=
console.log("inequality operator");
console.log(1!=1);
 // true
console.log(1!=2);
 // false
console.log(1!="1")
 // false. data-type
 console.log(0!=false);
 // false. number-bolean
console.log("juan"!="JUAN");
// false, js is case-sensitive
console.log("juan"!=juan);
// true

// strict inequality operator !=
console.log("inequality operator");
console.log(1!==1);
 // true
console.log(1!==2);
 // false
console.log(1!=="1")
 // false. data-type
 console.log(0!==false);
 // false. number-bolean
console.log("juan"!=="JUAN");
// false, js is case-sensitive
console.log("juan"!==juan);
// true



// relational comparison operator < >
let x = 500;
let y = 700;
let w = 8000;
let numString = "5500";

// greater than
console.log("greater than");
console.log(x > y);
// false

console.log("less than");
console.log(y < y);
// false

console.log(numString < 1000);
// forced coercion



// greater than
console.log("greater than or equal to");
console.log(x >= y);
// false

console.log("less than or equal to");
console.log(y <= y);
// false

console.log(numString <= 1000);
// forced coercion








// logical operators, && || !

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;
// is ng bolean

// logical And
console.log("Logical And Operator");
let authorization1 = isAdmin && isRegistered;
console.log(authorization1);
// true if every thing is true

let authorization2 = isRegistered && isLegalAge;
console.log(authorization2);

let requiredLevel = 95;
let requiredAge = 18;

authorization3 = isRegistered && requiredLevel === 95 && isLegalAge;
console.log(authorization3);


//Logical OR double pipe
 console.log("Logical Or Operator");
 // true if atleast 1 is true

 let userLevel = 100;
 let userLevel2 = 65;

 let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge >= requiredAge;
 console.log(guildRequirement1);



// logical not operator
 console.log("Logical Not Operator");
// turns a bolean into the opposite value

let opposite = !isAdmin;
console.log(opposite);
// isAdmin is false, not turns it into true

console.log(isRegistered);
// true
console.log(!isRegistered);



let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);






// if else if and if statement

// if statement
if(true) {
	console.log("we just run an if condition!")
};

let numG = 5;

if(numG < 10) {
	console.log("hello");
};

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

// length =  characters
if(userName3.length > 10) {
	console.log("welcome to game online!")
}

if(userName3.length >= 10 && isRegistered && isAdmin) {
	console.log("welcome to the jungle!")
}

// else statement
// execute statement if all other conditions are false

if(userName3.length >= 10 && isRegistered && isAdmin) {
	console.log("welcome to the jungle!")
} else{
	console.log("you are not ready!")
};


if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge) {
	console.log("thanks")
} else {
	console.log("sorry boy")
};

// else if statement
// if previous conditions are false


if(userName3.length <= 10 && userLevel3 >= 25 && userAge3 >= requiredAge) {
	console.log("yeah boy")
} else if(userLevel3 > 25) {
	console.log("too strong")
} else if(userAge3 < requiredAge){
	console.log("you are too young")
} else {
	console.log("you are not ready")
};


console.log("functions");






// if, else if and else statement with functions

function addNum(num1, num2){
	// check if the numbers are passed as number types
	// ang nilalabas ni typeof ay string "number"
	if(typeof num1 === "number" && typeof num2 === "number") {
		console.log("run if bith are number types")
		console.log(num1 + num2)
	} else {
		console.log("one or both")
	};
}

addNum(5,"2")
// else false

addNum(5,2)
// if, 7 true









console.log("create log in function");

// check if username and pw is string typeof


// function login(username, password){
// 	if(typeof username === "string" && typeof password === "string") {
// 		console.log("both are strings")

// 	} else {
// 		console.log("one or both")
// 	}
// }

// login("jane","jane23");




/*console.log("miniActivity");

function login(username, password){
	if(typeof username === "string" && typeof password === "string")
	{
		console.log("both are strings")

		if(username.length >= 8 && password.length >= 8) {
			alert("Thank you for logging in.")
		} else if(username.length >= 8) {
			alert("password too short")

		} else if(username.length >= 8) {
			alert("username too short")

		} 
		else {
			alert("Credentials too short.")
		}
		
	}
	 // else {
		// 	console.log("Credentials too short.")
		// }
}*/

/*login("janasdasdr","jaasdasde23");*/


// return
console.log("return key word");

let message = "no message";
console.log(message);

function determinTyphoonIntenstity(windspeed) {
	if(windspeed < 30){
		console.log("not a typhoon yet");
	} else if(windspeed <= 61) {
		console.log("TD detected")
	} else if(windspeed >= 62 && windspeed <= 88) {
		console.log("TS detected")
	} else if(windspeed >= 89 && windspeed <= 117) {
		console.log("STS detected")
	} else{
		return "Typhoon detected"
	}
}

message = determinTyphoonIntenstity(168);
console.log(message);

// mesage = no message
// return : pinalitan si no message
// message naging determine typhoon

if(message =="Typhoon detected"){
	console.warn(message);
} else{
	console.log("relax")
}
// alert . warn if TRUE


// truthy

if(true) {
	console.log("truthy")
} else{
	console.log("falsy")
};

if(0) {
	console.log("truthy")
} else{
	console.log("falsy")
};





// template literals

let fName = "jane";
let mName = "doe";
let lName = "smith";

console.log(fName + " " + mName + " " + lName)

console.log(`${fName} ${mName} ${lName}` )


// ternary operators es6

/*
syntax: 
	(expression/condition) ? ifTrue : ifFalse;
*/


let ternaryREsult = (1 > 18) ? "pera" : "bayong";
console.log(ternaryREsult);


500 > 1000 ? console.log("ok") : console.log("lugi ako")




// else if
let a = 10;

a === 5
? console.log("A")
: (a === 10 ? console.log("A is 10") : console.log("A is not 10"));


// else



// multiple statement operator
/*let name;

function isOfLegalAge() {
	name = "john"
	return "you are of legal age limit"
}

function isUnderAge() {
	name = "jane"
	return "you are under the age limit"
}


let age = parseInt(prompt("what is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log(`"result of the ternary operator in functions: " ${legalAge}, ${name}`)


*/
// switch statement
	// alternative for if-else if-else statement
	/*
		switch(expression){
			case value1:
				stement; - display etc.
				break;	- nagmatch si expression - to stop
			case value2:
				statement;
				break;
			case valueN:
				statement;
				break;
			default:
				statement;
		}
	*/


	// switch can not accept logical operators
	let day = prompt("what day of the week is it today?").toLowerCase();
	console.log(day);

	switch(day){
		case "monday":
			console.log("the color of the day is red.")
			break;
		case "tuesday":
			console.log("the color of the day is orange.")
			break;
		case "wednesday":
			console.log("the color of the day is yellow.")
			break;
		case "thursday":
			console.log("the color of the day is green.")
			break;
		case "friday":
			console.log("the color of the day is blue.")
			break;
		case "saturday":
			console.log("the color of the day is indigo.")
			break;
		case "sunday":
			console.log("the color of the day is violet.")
			break;
		default:
			alert("please input a valid day.");
			// prompt("what day of the week is it today?").toLowerCase();
	// 		// ?????????? why no work?
	}


	// try-catch-finally statement
		// used to test codes
		// try to execute the code block
		// catch displays the error
		// finally executes order disregarding the error




 /* range of values in case option inside the 
 Javascript switch statement */
 
let xX = prompt("Enter wind speed:");

switch (true) {
    case (xX <25):
        alert("not a strong wind");
        break;
    case (xX >= 25 && x < 39):
        alert("strong wind");
        break;
    case (xX >= 39 && x < 55):
        alert("gale");
        break;
     case (xX >= 55 && x < 73):
        alert("whole gale");
        break;
    default:
        alert("hurricane");
        break;
}
// ==========================
























// function showIntensityAlert(windspeed){
// 	try{
// 		// attempt to execute code
// 		alerta(determinTyphoonIntenstity(windspeed));
// 	}
// 	catch(error){
// 		// store code errors in the error
// 		console.log(typeof error);
// 		console.log(error.message);
// 		// alerta is not defined
// 	}
// 	finally{
// 		// tries to execute the error regardless of the errors
// 		alert("intensity updates will show alert")
// 	}
// }

// showIntensityAlert(56);


